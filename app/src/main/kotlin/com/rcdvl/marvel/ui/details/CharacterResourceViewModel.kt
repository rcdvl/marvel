package com.rcdvl.marvel.ui.details

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.rcdvl.marvel.model.MarvelResource
import com.rcdvl.marvel.model.MarvelResponse
import com.rcdvl.marvel.networking.MarvelService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by renan on 18/02/2018.
 */
class CharacterResourceViewModel(private val marvelService: MarvelService,
                                 private val resourceType: String) : ViewModel() {

    private val currentCalls = mutableListOf<Call<MarvelResponse<MarvelResource>>>()
    val charactersLiveData = MutableLiveData<ArrayList<MarvelResource>>()
    var offset = 0
    val count = 20
    var total = Int.MAX_VALUE
    var loading = false
    var isFirstRequest = true

    private val callback = object : Callback<MarvelResponse<MarvelResource>> {
        override fun onResponse(call: Call<MarvelResponse<MarvelResource>>?,
                                response: Response<MarvelResponse<MarvelResource>>?) {
            val responseData = response?.body()?.data ?: return

            total = responseData.total

            offset += count
            synchronized(currentCalls) {
                currentCalls.remove(call)
            }
            loading = false
            isFirstRequest = false

            if (charactersLiveData.value == null) {
                charactersLiveData.value = responseData.results
            } else {
                charactersLiveData.value!!.addAll(responseData.results)
                charactersLiveData.postValue(charactersLiveData.value)
            }
        }

        override fun onFailure(call: Call<MarvelResponse<MarvelResource>>?, t: Throwable?) {
            charactersLiveData.postValue(null)
            synchronized(currentCalls) {
                currentCalls.remove(call)
            }
            loading = false
        }
    }

    fun loadMore(characterId: Long) {
        fireCall(marvelService.getCharacterResourceList(resourceType, characterId, offset, count))
    }

    fun reset() {
        offset = 0
        total = Int.MAX_VALUE
        loading = false
    }

    private fun fireCall(call: Call<MarvelResponse<MarvelResource>>) {
        synchronized(currentCalls) {
            currentCalls += call
        }
        call.enqueue(callback)
        loading = true
    }
}