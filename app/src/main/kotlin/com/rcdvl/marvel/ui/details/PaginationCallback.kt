package com.rcdvl.marvel.ui.details

import com.paginate.Paginate

/**
 * Created by Renan on 22/03/2016.
 */
class PaginationCallback(private val characterId: Long,
                         private val marvelService: CharacterResourceViewModel) : Paginate.Callbacks {

    override fun onLoadMore() {
        marvelService.loadMore(characterId)
    }

    override fun isLoading() = marvelService.loading

    override fun hasLoadedAllItems(): Boolean {
        return if (marvelService.isFirstRequest) {
            false
        } else {
            marvelService.offset >= marvelService.total
        }
    }
}