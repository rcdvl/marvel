package com.rcdvl.marvel.ui.list

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.rcdvl.marvel.model.MarvelCharacter
import com.rcdvl.marvel.model.MarvelResponse
import com.rcdvl.marvel.networking.MarvelService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by renan on 10/9/17.
 */
class CharacterListViewModel(val marvelService: MarvelService) : ViewModel() {

    private var lastSearchTerm = ""
    private val currentCalls = mutableListOf<Call<MarvelResponse<MarvelCharacter>>>()
    val charactersLiveData = MutableLiveData<ArrayList<MarvelCharacter>>()
    var offset = 0
    val count = 20
    var total = Int.MAX_VALUE
    var loading = false

    private val callback = object : Callback<MarvelResponse<MarvelCharacter>> {
        override fun onResponse(call: Call<MarvelResponse<MarvelCharacter>>?,
                                response: Response<MarvelResponse<MarvelCharacter>>?) {
            val responseData = response?.body()?.data ?: return

            total = responseData.total

            offset += count
            synchronized(currentCalls) {
                currentCalls.remove(call)
            }
            loading = false

            if (charactersLiveData.value == null) {
                charactersLiveData.value = responseData.results
            } else {
                charactersLiveData.value!!.addAll(responseData.results)
                charactersLiveData.postValue(charactersLiveData.value)
            }
        }

        override fun onFailure(call: Call<MarvelResponse<MarvelCharacter>>?, t: Throwable?) {
            charactersLiveData.postValue(null)
            synchronized(currentCalls) {
                currentCalls.remove(call)
            }
            loading = false
        }
    }

    fun loadMoreCharacters(searchTerm: String? = null) {
        searchTerm?.let {
            if (it != lastSearchTerm) {
                lastSearchTerm = it
                reset()

                synchronized(currentCalls) {
                    currentCalls.forEach {
                        it.cancel()
                        currentCalls.remove(it)
                    }
                }
            }

            fireCall(marvelService.getCharactersList(offset, count, searchTerm))
            return
        }

        fireCall(marvelService.getCharactersList(offset, count))
    }

    fun reset() {
        offset = 0
        total = Int.MAX_VALUE
        charactersLiveData.value = arrayListOf()
        loading = false
    }

    private fun fireCall(call: Call<MarvelResponse<MarvelCharacter>>) {
        synchronized(currentCalls) {
            currentCalls += call
        }
        call.enqueue(callback)
        loading = true
    }
}