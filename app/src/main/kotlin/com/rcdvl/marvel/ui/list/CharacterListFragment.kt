package com.rcdvl.marvel.ui.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.paginate.Paginate
import com.rcdvl.marvel.MarvelApplication
import com.rcdvl.marvel.R
import com.rcdvl.marvel.model.MarvelCharacter
import com.rcdvl.marvel.ui.MarvelViewModelFactory
import kotlinx.android.synthetic.main.fragment_character_list.*
import java.util.*
import javax.inject.Inject

/**
 * Created by renan on 3/17/16.
 */
class CharacterListFragment : Fragment() {
    private lateinit var viewModel: CharacterListViewModel
    private val adapter = CharactersAdapter()
    @Inject lateinit var marvelViewModelFactory: MarvelViewModelFactory

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        (activity.application as MarvelApplication).appComponent.inject(this)

        viewModel = ViewModelProviders.of(this,
                marvelViewModelFactory)[CharacterListViewModel::class.java]
        viewModel.charactersLiveData.observe(this,
                Observer<ArrayList<MarvelCharacter>> { characters: ArrayList<MarvelCharacter>? ->
                    if (characters == null) {
                        // show network error
                        return@Observer
                    }

                    val count = adapter.characters.size
                    adapter.characters = characters
                    adapter.notifyItemRangeInserted(count + 1, viewModel.count)
                })
        return inflater?.inflate(R.layout.fragment_character_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        val layoutManager = LinearLayoutManager(context)
        charactersList.layoutManager = layoutManager
        charactersList.setHasFixedSize(true)
        charactersList.adapter = adapter
        adapter.characters = viewModel.charactersLiveData.value ?: arrayListOf()

        val callbacks = object : Paginate.Callbacks {
            override fun onLoadMore() {
                viewModel.loadMoreCharacters()
            }

            override fun isLoading() = viewModel.loading

            override fun hasLoadedAllItems() = viewModel.offset >= viewModel.total
        }

        Paginate.with(charactersList, callbacks).setLoadingTriggerThreshold(2).addLoadingListItem(
                true).setLoadingListItemCreator(CharacterListLoadingItemCreator()).build()

        super.onViewCreated(view, savedInstanceState)
    }

}
